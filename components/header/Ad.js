// import styles from "./styles.module.scss";
// import Link from "next/link";

// export default function Ad() {
//   return (
//     <Link href="/browse" >
//         <div className={styles.ad}></div>
//     </Link>
//   );

// }
import Link from "next/link";
import React from "react";
import styles from "./styles.module.scss";
export default function Ad() {
  return (
    <Link legacyBehavior href="/browse">
      <div className={styles.ad}></div>
    </Link>
  );
}
